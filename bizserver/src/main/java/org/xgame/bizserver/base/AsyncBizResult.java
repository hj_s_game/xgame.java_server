package org.xgame.bizserver.base;

import org.xgame.comm.MainThreadProcessor;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

/**
 * 异步业务结果
 *
 * @param <R> 返回值类型
 */
public final class AsyncBizResult<R> {
    /**
     * 是否已有返回值回调
     */
    private final AtomicBoolean _hasConsumer = new AtomicBoolean();

    /**
     * 返回值回调
     */
    private Consumer<R> _consumer;

    /**
     * 是否已有返回值
     */
    private final AtomicBoolean _hasReturnVal = new AtomicBoolean();

    /**
     * 返回值
     */
    private R _returnVal;

    /**
     * 设置返回值, 只能设置一次
     *
     * @param val 返回值
     */
    public void putReturnVal(final R val) {
        if (_hasReturnVal.compareAndSet(false, true)) {
            _returnVal = val;
            doConsume();
        }
    }

    /**
     * 设置返回值回调, 回调只能被执行一次
     *
     * @param val 返回值回调
     */
    public void returnValFunc(Consumer<R> val) {
        if (null != val &&
            _hasConsumer.compareAndSet(false, true)) {
            _consumer = val;
            doConsume();
        }
    }

    /**
     * 执行返回值回调
     */
    private void doConsume() {
        if (null != _consumer) {
            MainThreadProcessor.getInstance().process(() -> _consumer.accept(_returnVal));
        }
    }
}
