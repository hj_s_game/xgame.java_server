package org.xgame.bizserver.module.login;

import org.xgame.bizserver.base.AsyncBizResult;

/**
 * 登录服务
 */
public final class LoginService implements
    IServ_doLoginByGuestCode {
    /**
     * 单例对象
     */
    private static final LoginService INSTANCE = new LoginService();

    /**
     * 私有化类默认构造器
     */
    private LoginService() {
    }

    /**
     * 获取单例对象
     *
     * @return 单例对象
     */
    public static LoginService getInstance() {
        return INSTANCE;
    }

    /**
     * 执行登录过程
     *
     * @param loginMethod 登录方法
     * @param propertyStr 属性字符串
     * @return 异步业务结果
     */
    public AsyncBizResult<String> doLogin(int loginMethod, String propertyStr) {
        if (null == propertyStr ||
            propertyStr.isEmpty()) {
            return null;
        }

        return this.doLoginByGuestCode(propertyStr);
    }
}
