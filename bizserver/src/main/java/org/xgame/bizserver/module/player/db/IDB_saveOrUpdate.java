package org.xgame.bizserver.module.player.db;

import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.xgame.bizserver.base.MongoClientSingleton;
import org.xgame.bizserver.def.MongoDef;
import org.xgame.bizserver.module.player.model.PlayerModel;

import static com.mongodb.client.model.Filters.eq;

interface IDB_saveOrUpdate {
    /**
     * 保存和更新
     *
     * @param p 玩家模型
     */
    default void saveOrUpdate(PlayerModel p) {
        if (null == p) {
            return;
        }

        Bson cond = eq("playerUUId", p.getUUId());
        Document doc = new Document();
        doc.put("$set", Document.parse(p.toJSON().toJSONString()));

        UpdateOptions opt = new UpdateOptions().upsert(true);

        MongoClientSingleton.getInstance()
            .getDatabase(MongoDef.DB_XXOO)
            .getCollection(MongoDef.COLLECTION_PLAYER)
            .updateOne(cond, doc, opt);
    }
}
